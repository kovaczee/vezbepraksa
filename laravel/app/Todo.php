<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
    protected $table = 'todos';

    public static $rules = [
        'todo' => 'required|max:255',
        'priority' => 'required',

    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['todo', 'priority', 'done'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
