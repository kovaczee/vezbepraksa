<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('pages.home');
});


Route::get('/aaa', function () {
    return 'dddddd';

});



Route::get('hello', ['uses' => 'HelloController@hello', 'as' => 'name']);



Route::group(['middleware' => 'auth'], function () {
    // Authentication routes...
    Route::get('auth/logout', 'Auth\AuthController@getLogout');

    Route::get('/home', ['uses' => 'HomeController@index']);
    Route::resource('todos', 'TodosController');
});

Route::group(['middleware' => 'guest'], function () {
    // Authentication routes...
    Route::get('auth/login', 'Auth\AuthController@getLogin');
    Route::post('auth/login', 'Auth\AuthController@postLogin');

    // Registration routes...
    Route::get('auth/signup', 'Auth\AuthController@getRegister');
    Route::post('auth/signup', 'Auth\AuthController@postRegister');
});

