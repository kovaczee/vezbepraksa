<?php

namespace App\Http\Controllers;

use App\Todo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class TodosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Auth::user()->todos;

        //return view('pages.home');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate the request...
        $validator =  $this->validates($request->all());
        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $todo = new Todo;
        $todo->todo = $request->todo;
        $todo->user_id = Auth::user()->id;
        $todo->priority = $request->priority;
        $todo->done = false;
        $todo->save();
        return $todo;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Auth::user()->todo->find($id);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator =  $this->validates($request->all());
        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }
        $todo = Todo::find($id);

        if ($todo->user_id === Auth::user()->id) {
            $todo->todo = $request->todo;
            $todo->priority = $request->priority;
            $todo->done = $todo->done;

            $todo->save();

            return $todo;
        } else {
            return abort(400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $todo = Auth::user()->todos->find($id);

        if ($todo) {
            if ($todo->delete()) {
                return response('OK');
            } else {
                return abort(400);
            }
        } else {
            return abort(403);
        }
    }

    protected function validates(array $data)
    {

        return Validator::make($data, Todo::$rules);
    }


}
