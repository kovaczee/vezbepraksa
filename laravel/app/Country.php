<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'countries';
    //echo Form::select('country', array('US' => 'United States', 'UK' => 'United Kingdom'));

    protected $fillable = ['name'];

    //public static function all()
    //{
      //  return array_keys($this->countries);
    //}
}
