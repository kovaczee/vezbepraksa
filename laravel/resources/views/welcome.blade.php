<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">Laravel</div>
            </div>
        </div>


        <!-- testing anonimus functions -->
        <script  src="js/vezbe.js" type="text/javascript"></script>

        <script>console.log(test);</script>

        <script>test="Script tag";</script>

        <script>console.log(test);</script>

        <script  src="js/variable.js" type="text/javascript"></script>
        <script>console.log(test);</script>

        <script type="text/javascript" src="js/vezbe.js"></script>
        <script>console.log(test);</script>

        <script type="text/javascript" src="js/variable.js"></script>
        <script>console.log(test);</script>

        <script type="text/javascript" src="/js/person.js"></script>
        <script></script>

    </body>
</html>
