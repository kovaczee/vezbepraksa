<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <meta charset="utf-8" />
    <script src="../js/vendors/angular.js"></script>
    <script src="../js/vendors/angular-route.js"> </script>
    <script src="../js/testapp.js"> </script>
    <script src="/js/controllers/testctrl.js"> </script>
</head>
<!-- end head -->
<body ng-app="Todo"> <!-- wrapper -->
<header>
    @include('layouts.header')
</header>

<div id="wrapper">
    @yield('content')
</div>
<button><a href="/#/todos">Show Todos</a></button>
<div ng-view></div>
<!-- end wrapper -->

<!-- footer -->
@include('layouts.footer')
        <!-- end footer -->
<script src="../js/controllers/todoCtrl.js"></script>
<script src="https://code.jquery.com/jquery-2.1.4.min.js" ></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>


</body>
</html>