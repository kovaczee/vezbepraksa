
<form method="POST" action="/auth/signup">
{!! csrf_field() !!}

<div>
    First Name
    <input type="text" name="firstname" value="{{ old('firstname') }}" required>
</div>

<div>
    Last Name
    <input type="text" name="lastname" value="{{old('lastname') }}" required>
</div>

<div>
    Email
    <input type="email" name="email" value="{{ old('email') }}" required>
</div>

<div>
    Password
    <input type="password" name="password" required>
</div>

    <div>
        Confirm Password
        <input type="password" name="password_confirmation">
    </div>

<div>
    Company
    <input type="company" name="company" value="{{ old('company') }}" required>
</div>

<div>
    Country
    <select name="country_id">
        @foreach (\App\Country::all() as $country)
            <option value="{{ $country->id }}">{{ $country->name }}</option>
        @endforeach


    </select>
</div>



<div>
    <button type="submit">Register</button>
</div>
</form>