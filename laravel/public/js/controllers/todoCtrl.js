/**
 * Created by kovaczee on 31.12.15..
 */
var app = angular.module('Todo', ['ngRoute']);

app.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    $routeProvider.
    when('/todos', {
        templateUrl: '/templates/todoApp.html',
        controller: 'TodoCtrl'
    });
}]);

app.controller('TodoCtrl', function($scope, $http) {

    $scope.priorities = [
        { number: 0, name: 'Low'},
        { number: 1, name: 'Medium'},
        { number: 2, name: 'High'}
    ];

    $scope.priority = 0;

    $http.get("/todos")
        .then(function (response) {
            $scope.todos = response.data;
        });

    $scope.addTodo = function () {
        var dataObj = {
            todo: $scope.todoDescription,
            priority: $scope.priority,
            done: false
        };

        $http.post("/todos", dataObj)
            .success(function (response) {
                $scope.todos.push(response);
                $scope.todoDescription = "";
                $scope.priority = 0;
                $scope.clearErrorMessage();

            }).
            error(function (response) {
                $scope.sendErrorResponse(response);
        });

    };

    $scope.clearErrorMessage = function() {
        $scope.todoError = null;
        $scope.priorityError = null;
    }

    $scope.sendErrorResponse = function(response) {
        $scope.todoError = response.todo ? response.todo[0] : null;
        $scope.priorityError = response.priority ? response.priority[0] : null;
    }

    $scope.deleteTodo = function(todo) {
        $http.delete("/todos/" + todo.id)
            .success(function(data) {
                var index = $scope.todos.indexOf(todo);
                $scope.todos.splice(index, 1);
            }).error(function(data) {
                console.log('Error: ' + data);
        });

    }

    $scope.editTodo = function(todo) {
        $http({
            method: 'PUT',
            url: '/todos/' + todo.id,
            data: todo
        }).success(function(response) {
                todo.editMode = false;
                $scope.clearErrorMessage();
        }).error(function (response) {
            $scope.sendErrorResponse(response);
        });
    }

    $scope.setEditMode = function(todo, value) {
        todo.editMode = value;
        if (value) {
            $scope.oldTodo = angular.copy(todo);
        } else {
            var index = $scope.todos.indexOf(todo);
            $scope.oldTodo.editMode = value;
            $scope.todos[index] = angular.copy($scope.oldTodo);
        }
    }

});
