function Person(firstName, lastName, gender){
    this.firstName = firstName;
    this.lastName = lastName;
    this.gender = gender;

}
//static method
Person.print=function() {
    console.log('This is some kind of a print method!');
}
Person.print();

Person.prototype.toString=function() {
    return '[Person ' + this.firstName + ' ' + this.lastName + ' ' + this.gender + ']';
}

//static variable
Person.counter = 0;
console.log(Person.counter);

Practicant.prototype = new Person();        // Here's where the inheritance occurs
Practicant.prototype.constructor=Practicant;       // Otherwise instances of Practicant would have a constructor of Person
function Practicant(firstName, lastName, gender){
    this.firstName = firstName;
    this.lastName = lastName;
    this.gender = gender;
}

Practicant.prototype.toString=function() {
    return '[Practicant ' + this.firstName + ' ' + this.lastName + ' ' + this.gender + ']';
}


var newPerson = new Person("Zoran", "Kovacevic", "male");
var newPracticant = new Practicant('Felix', 'Macak', 'zivotinja');

console.log('new person is ' +newPerson);
console.log('new practicant ' +newPracticant);
