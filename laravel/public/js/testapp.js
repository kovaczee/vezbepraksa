var app = angular.module('Main', ['ngRoute']);

app.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    $routeProvider.
    when('/content/first', {
        templateUrl: '/templates/test.html',
        controller: 'first'
    }).
    when('/content/second', {
        templateUrl: '/templates/testing.html',
        controller: 'TestCtrl'
    });
}]);

app.controller('first', function($scope) {
    $scope.list = [1,2,3,4,5];
});
