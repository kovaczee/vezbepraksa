<?php

use Illuminate\Database\Seeder;
use App\Todo;

class TodosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Todo::truncate();
        // add 1st row
        Todo::create( [
            'todo' => 'start the last exercise' ,
            'priority' => 1 ,
            'done' => false
        ] );
        // add 2nd row
        Todo::create( [
            'todo' => 'do as much as you can today' ,
            'priority' => 1 ,
            'done' => false
        ] );
    }
}
